Why Aspose for VSTO
===============
The developers looking for file format libraries may need to walk through the features provided by the libraries available on the market. Mainly, two factors are considered when choosing an option:

* Features
* Efforts required to use the library

If you are a .NET developer looking for faster,easy and light-weight file format library to process files, you may need to compare VSTO(Visual Studio tool for Office) with Aspose. In this case, this project will help you to compare features and code in both libraries.Please visit [project documentation]() for a comprehensive comparison.In addition, there are important [automation drawbacks](http://www.aspose.com/docs/display/wordsnet/Why+not+Automation) when using VSTO for server side file processing.


### VSTO: ###
VSTO, or Visual Studio Tools for Office, or Microsoft Visual Studio Tools is part of Microsoft's Visual Studio .NET tools suite and supports the Microsoft .NET Framework. It is used by developers to write code connecting Microsoft Office Word, Excel and Outlook applications. Developers using VSTO can employ C# or Visual Basic languages. It is an alternative to Visual Basic for Applications, or VBA.

### What is the use of Aspose .NET Products? ###

[Aspose](http://www.aspose.com) are file format experts and provide APIs and components for various file formats including MS Office, OpenOffice, PDF and Image formats. These APIs are available on a number of development platforms including .NET
 frameworks &ndash; the .NET frameworks starting from version 2.0 are supported. If you are a .NET developer, you can use Aspose’s native .NET APIs in your .NET applications to process various file formats in just a few lines of codes. All the Aspose
 APIs don’t have any dependency over any other engine. For example, you don’t need to have MS Office installed on the server to process MS Office files... [Continue reading →](https://bitbucket.org/asposemarketplace/aspose-for-vsto/wiki/What%20is%20the%20use%20of%20Aspose%20.NET%20Products%3F)
